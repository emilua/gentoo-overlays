EAPI=8
EGIT_REPO_URI="https://gitlab.com/emilua/glib"
inherit meson
DESCRIPTION="GLIB event loop integration to emilua"

HOMEPAGE="http://emilua.org/"

SRC_URI="https://gitlab.com/emilua/glib/-/archive/v${PV}/glib-v${PV}.tar.gz"

S=${WORKDIR}/glib-v${PV}/

LICENSE="Boost-1.0"

SLOT="0"

KEYWORDS="~amd64"

DEPEND="dev-libs/glib
	dev-vcs/git
	dev-lang/emilua"
