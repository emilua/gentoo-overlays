EAPI=8
EGIT_REPO_URI="https://gitlab.com/emilua/glib"
inherit meson
DESCRIPTION="GLIB event loop integration to emilua"

HOMEPAGE="http://emilua.org/"

SRC_URI="https://gitlab.com/emilua/dumb-dbus/-/archive/v${PV}/dumb-dbus-v${PV}.tar.gz"

S=${WORKDIR}/dumb-dbus-v${PV}

LICENSE="Boost-1.0"

SLOT="0"

KEYWORDS="~amd64"

DEPEND="dev-libs/glib
	dev-vcs/git
	dev-lang/emilua-glib
	sys-apps/gawk
	dev-cpp/glibmm
	dev-lang/emilua"
