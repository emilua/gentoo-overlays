EAPI=8
EGIT_REPO_URI="https://gitlab.com/emilua/qt5"
inherit meson
DESCRIPTION="Emilua support for qt5 GUIs using lua"

HOMEPAGE="http://emilua.org/"

SRC_URI="https://gitlab.com/emilua/qt5/-/archive/v${PV}/qt5-v${PV}.tar.gz -> emilua_qt.tar.gz"

S=/var/tmp/portage/dev-lang/emilua-qt5-${PV}/work/qt5-v${PV}/

LICENSE="Boost-1.0"

SLOT="0"

KEYWORDS="~amd64"

DEPEND="dev-lang/emilua
	dev-vcs/git
	dev-util/gperf
	dev-qt/qtcore
	dev-qt/qtquickcontrols"

